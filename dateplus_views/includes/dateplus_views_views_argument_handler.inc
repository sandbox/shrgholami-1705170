<?php

/**
 * Date API argument handler.
 */
class dateplus_views_views_argument_handler extends date_views_argument_handler {

  /**
   * Inject a test for valid date range before the regular query.
   * Override the parent query to be able to control the $group.
   */
  function query($group_by = FALSE) {
    $this->get_query_fields();
    $this->query->set_where_group($this->options['date_method'], $this->options['date_group']);
    $this->granularity = $this->date_handler->arg_granularity($this->argument);
    $format = $this->date_handler->views_formats($this->granularity, 'sql');
    $this->orginal_argument = $this->argument;
    $this->argument = $this->convert_date($this->argument, $format);


    if ($this->date_forbid()) {
      return;
    }

    if (!empty($this->query_fields)) {
      // Use set_where_group() with the selected date_method
      // of 'AND' or 'OR' to create the where clause.
      foreach ($this->query_fields as $count => $query_field) {
        $field = $query_field['field'];
        $this->date_handler = $query_field['date_handler'];
        $this->field = $field['field_name'];
        $this->real_field = $field['field_name'];
        $this->table = $field['table_name'];
        $this->original_table = $field['table_name'];
        if ($field['table_name'] != $this->table || !empty($this->relationship)) {
          $this->table = $this->query->queue_table($field['table_name'], $this->relationship);
        }
        // $this->table_alias gets set when the first field is processed if otherwise empty.
        // For subsequent fields, we need to be sure it is emptied again.
        elseif (empty($this->relationship)) {
          $this->table_alias = NULL;
        }
      }
    }

    // See if we need to reset granularity based on an argument value.
    // Make sure we don't try to reset to some bogus value if someone has typed in an unexpected argument.
    $granularity = $this->date_handler->arg_granularity($this->argument);
    if (!empty($granularity)) {
      $this->date_handler->granularity = $granularity;
      $this->format = $this->date_handler->views_formats($this->date_handler->granularity, 'display');
      $this->sql_format = $this->date_handler->views_formats($this->date_handler->granularity, 'sql');
    }
    $this->granularity = $this->date_handler->granularity;
    $this->ensure_my_table();
    $group = !empty($this->options['date_group']) ? $this->options['date_group'] : 0;

    // If requested, add the delta field to the view so we can later find the value that matched our query.
    if (!empty($this->options['add_delta']) && (substr($this->real_field, -6) == '_value' || substr($this->real_field, -7) == '_value2')) {
      $this->query->add_field($this->table_alias, 'delta');
      $real_field_name = str_replace(array('_value', '_value2'), '', $this->real_field);
      $this->query->add_field($this->table_alias, 'entity_id', 'date_id_' . $real_field_name);
      $this->query->add_field($this->table_alias, 'delta', 'date_delta_' . $real_field_name);
    }

    $format = ($this->date_handler->granularity  != 'day') ? DATE_FORMAT_DATETIME : $this->sql_format;
    $view_min = $this->get_min_date($this->orginal_argument, $format);
    $view_max = $this->get_max_date($this->orginal_argument, $format);
    $view_min_placeholder = $this->placeholder();
    $view_max_placeholder = $this->placeholder();
    $this->date_handler->placeholders = array($view_min_placeholder => $view_min, $view_max_placeholder => $view_max);

    // Are we comparing this field only or the Start/End date range to the view criteria?
    if (!empty($this->options['use_fromto'])) {

      // The simple case, match the field to the view range.
      $field = $this->date_handler->sql_field($this->table_alias . '.' . $this->real_field, NULL, $this->min_date);
      $field = $this->date_handler->sql_format($format, $field);
      $this->query->add_where_expression($group, "$field >= $view_min_placeholder AND $field <= $view_max_placeholder", array($view_min_placeholder => $view_min, $view_max_placeholder => $view_max));

    }
    else {

      // Look for the intersection of the range of the date field with the range of the view.
      // Get the Start/End values for this field. Retrieve using the original table name.
      // Swap the current table name (adjusted for relationships) into the query.
      // @TODO We may be able to use Views substitutions here, investigate that later.
      $fields = date_views_fields($this->base_table);
      $fields = $fields['name'];
      $fromto = $fields[$this->original_table . '.' . $this->real_field]['fromto'];

      $value_min = str_replace($this->original_table, $this->table_alias, $fromto[0]);
      $value_max = str_replace($this->original_table, $this->table_alias, $fromto[1]);
      $field_min = $this->date_handler->sql_field($value_min, NULL, $this->min_date);
      $field_min = $this->date_handler->sql_format($format, $field_min);
      $field_max = $this->date_handler->sql_field($value_max, NULL, $this->max_date);
      $field_max = $this->date_handler->sql_format($format, $field_max);
      // You can check the built query with this
      //var_dump($group, "$field_max >= $view_min AND $field_min <= $view_max");exit;
      $this->query->add_where_expression($group, "$field_max >= $view_min_placeholder AND $field_min <= $view_max_placeholder", array($view_min_placeholder => $view_min, $view_max_placeholder => $view_max));
    }
  }

  protected function convert_date($date, $format) {
    return dateplus_convert_to_georgian($date, $format);
  }

  protected function get_min_date($date, $format) {
    return dateplus_get_first_month_day($date);
  }

  protected function get_max_date($date, $format) {
    return dateplus_get_last_month_day($date);
  }
}

