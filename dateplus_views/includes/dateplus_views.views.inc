<?php

/**
 * Implements hook_views_data()
 */
function dateplus_views_views_data() {
  $data = array();

  $tables = date_views_base_tables();
  foreach ($tables as $base_table => $entity) {
    // The flexible date argument.
    $data[$base_table]['dateplus_argument'] = array(
      'group' => t('Date'),
      'title' => t('Date+ (!base_table)', array('!base_table' => $base_table)),
      'help' => t('Filter any Views !base_table date field by a date argument, using any common ISO date/period format (i.e. YYYY, YYYY-MM, YYYY-MM-DD, YYYY-W99, YYYY-MM-DD--P3M, P90D, etc). ', array('!base_table' => $base_table)),
      'argument' => array(
        'handler' => 'dateplus_views_views_argument_handler',
        'empty field name' => t('Undated'),
        'is date' => TRUE,
      ),
    );
  }
 return $data;
}
